export const fields = products => {
    // Initialize: output will be the final response
    let output = [];
    // Future: list all fields here. This mimic database input. In case of future admin panel
    let list = {
      todo: {
        value: "todo",
        label: "To Do"
      },
      Patch: {
        value: "Patch",
      },
      Minor: {
        value: "Minor",
      },
      Major: {
        value: "Major",
      },
     
    };
    products.map(o => {
      // Get: get the field base on the option
      let field = list[o];
      // Assign: if field existing, push it into output;
      return field && output.push(field);
    });


    return output;
  };

  export const keys = product => {
    // Future: output product and not list directly, in case of future middleware
    let output = "";
    let list = {
      // Keys: this will fetch fields, respectivly
      sematic_versioning: ["todo", "Patch", "Minor", "Major", "First"],
      status: ["Pending", "Complete", "Erased"],
      // Future: default for easy reusability. Another platform may use default.
      default: []
    };
    // Default: return default from list, instead of manually adding array default as in []
    output = list[product] ? list[product] : list["default"];
    return output;
  };

  export const GetFields = (product) => {
    return fields(keys(product))
  }
