import React from "react";
import { Col, Icon, Card, Row, Statistic } from "antd";

export const StatPayload = entries => {
  // Convert entries object to an array ['key', {}]
  let list_entries = Object.entries(entries);
  // get the length of the pending area
  let now = list_entries.filter(entry => entry[1].status === "pending").length;
  return [
    {
      now: now,
      then: list_entries.length,
      title: "PENDING STATS",
      type: "count",
      message: (f, c) => {
        let plural = c > 1 ? " items" : " item";
        let bot = "";
        if (f >= 50) {
          bot =
            "Chop chop man. You are as lazy as my project manager from my last job.";
        } else if (f > 25) {
          bot = "Come on man, really!!! Get to work!!!";
        } else if (f > 0) {
          bot = "You almost done.";
        } else {
          bot =
            "Good Job. You should work for Genius Plaza. They can use a fast worker like yourself.";
        }

        return "*You have  " + c + plural + " pending. " + bot;
      },
      reverse: true
    }
  ];
};

export const StatisticCard = props => {
  let { now, then, message, title } = props;
  // avoid NaN
  var now_stat = now;
  var then_stat = then;
  // 0 based the stats
  if (!now_stat && !then_stat) {
    now_stat = 0;
    then_stat = 1;
  }
  // whole number stats
  let stat_value = (now_stat / then_stat) * 100;
  // visualized application
  let color = stat_value <= 0 ? "#3f8600" : "#8B0000";
  let arroy_type = stat_value < 50 ? "like" : "dislike";

  arroy_type = stat_value === 100 ? "dislike" : arroy_type;

  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
      <Card>
        <div className="inline">
          <Statistic
            title={title}
            value={stat_value}
            precision={2}
            valueStyle={{ color }}
            prefix={<Icon type={arroy_type} />}
            suffix={"% pending"}
          />
          {/* {title} */}
        </div>
        <div className="count_title inline push-right">{now}</div>
        <div className={"full_width_div "}>{message(stat_value, now)}</div>
      </Card>
    </Col>
  );
};

export const StatisticBoard = props => {
  let { list } = props;
  list = list ? list : [];
  let stats = StatPayload(list);
  return (
    <div style={{ background: "#ECECEC", padding: "2px" }}>
      <Row gutter={24}>
        {stats.map((value, index) => {
          return <StatisticCard key={index} {...value} />;
        })}
      </Row>
    </div>
  );
};
