import React from 'react';
import List from '../components/list';
import AddNewSection from '../components/addNewSection'
import { StatisticBoard } from '../utils/stats';
import Header from '../components/header';
import { Col,  Row} from 'antd';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      Patch: 0,
      Minor: 0,
      Major: 0,
      Id: 0,
      current: {
        version: 'todo',
        status: 'pending'
      },
      list: {}
    };
    this.onChange = this.onChange.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
    this.onSelectVersion = this.onSelectVersion.bind(this);
    this.handleOnRemove = this.handleOnRemove.bind(this);
    this.handleOnAdd = this.handleOnAdd.bind(this);
  }

  handleOnAdd() {
    // Check: make sure field is not empty
    if (this.state.current.value) {
      let current = {
        ...this.state.current,
        // add one to the current version
        [this.state.current.version]:
          this.state[this.state.current.version] + 1,
        created_at: Date.now()
      };
      // remove trailing spaces
      let trimmed_value = current.value.trim();
      current.value = trimmed_value;

      this.setState({
        list: { ...this.state.list, [this.state.Id]: current },
        [current.version]: this.state[current.version] + 1,
        Id: this.state.Id + 1,
        // reset cyrrebt state
        current: {
          version: 'todo',
          status: 'pending'
        }
      });
    }
  }
  handleOnRemove(key) {
      //DELETE: get the list and delete it based on key
    let list = this.state.list;
    delete list[key];
    this.setState({
      list
    });
  }
  onChange(event) {
    let value = event.target.value;
    this.setState({
      current: { ...this.state.current, value }
    });
  }
  onSelectVersion(value) {
    this.setState({
      current: { ...this.state.current, version: value }
    });
  }
  handleChangeStatus(id, value) {
    // get current based on ID. Add updated date.
    let current = this.state.list[id];
    current.status = value;
    this.setState({
      current: { ...this.state.current, [id]: current, updated_at: Date.now() }
    });
  }

  render() {
    let span = {
      xs: 24,
      sm: 24,
      md: 24,
      lg: 12,
      xl: 12,
      
    };
    //Format Version as '1.3.0'
    let current_version =  this.state.Major + '.' + this.state.Minor + '.' + this.state.Patch;

    return (
      <Row gutter={24}>
        <Col className='' span={18} offset={3}>
          <Header />
          <Row gutter={24}>
            <Col className='large-margin-bottom'  {...span}>
              <StatisticBoard list={this.state.list} />
            </Col>
            <Col className='large-margin-bottom' {...span} lg-push={6} >

              <AddNewSection handleOnAdd={this.handleOnAdd} current_value={this.state.current.value} onSelectVersion={this.onSelectVersion} onChange={this.onChange} />
              
              <List list={this.state.list} current_id = {this.state.Id} current_version={current_version} handleChangeStatus={this.handleChangeStatus} handleOnRemove={this.handleOnRemove}/>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default App;
