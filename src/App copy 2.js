import React from "react";
import formatDate from 'date-fns/distance_in_words_to_now';


import "./styles//App.scss";
import { Col, Icon, Card, Input, Row, Select, Statistic, Tag } from "antd";
const { Option } = Select;

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      Patch: 0,
      Minor: 0,
      Major: 0,
      Id: 0,
      current: {
        version: "todo",
        status: 'pending'
      },
      list: {}
    };
    this.onChange = this.onChange.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
    this.onSelectVersion = this.onSelectVersion.bind(this);
  }

  handleOnAdd(new_list) {
    if(this.state.current.value ){
    let current = {
      ...this.state.current,
      [this.state.current.version]: this.state[this.state.current.version] + 1,
      created_at: Date.now()
    };
    let trimmed_value = current.value.trim();
    current.value = trimmed_value

    this.setState({
      list: { ...this.state.list, [this.state.Id]: current },
      [current.version]: this.state[current.version] + 1,
      Id: this.state.Id + 1,
      current: {
        version: "todo",
        status: 'pending'
      },

    });
  }
    
  }
  handleOnRemove(key) {
    let list = this.state.list;
    delete list[key];
    this.setState({
      list
    });
  }
  onChange(event) {
    // console.log(event.target.value, this.state)
    let value = event.target.value 
    this.setState({
      current: { ...this.state.current, value }
    });
  }
  onSelectVersion(value) {
    // console.log(value, this.state)

    this.setState({
      current: { ...this.state.current, version: value }
    });
  }
  handleChangeStatus(id, value) {
    // console.log(value, this.state)
    let current = this.state.list[id]
    current.status = value

    this.setState({
      current: { ...this.state.current, [id]: current, updated_at:  Date.now() }
    });
  }

  render() {
    const FixedSelect = props => {
      // Default: default classname if undefined
      let className = props.className ? props.className : "";
      // Error Handling: make sure options is an array
      let options = Array.isArray(props.options) ? props.options : [];
      // Default: initialized onSelect function and default if none
      let callback = props.onSelect ? props.onSelect : f => f;
      let size = props.size ? props.size : ''
      return (
        // Default: defautl to patch. Assuming most of the to do will be a patch
        <Select size={size} defaultValue="Patch" className={className} onChange={callback}>
          {options.map((o, i) => {
            // Default: value should be defaulted to index if unassigned. Starting at 1, not 0
            let value = o.value ? o.value : i + 1;
            // Default: if no label, default it to value. Which should at least have a numerical value.
            let label = o.label ? o.label : value;
            return (
              <Option key={i} value={value}>
                {label}
              </Option>
            );
          })}
        </Select>
      );
    };

    let scaffold = {
      0: {
        value: "Developer must split components into seprate file",
        value: "Research best practice for components, containers, and utils",
        value:
          "Hint Hint: A Container does data fetching and then renders its corresponding components.",
        value:
          "Hint Hint:  must include a README describing the general implementation and the build process if required (look at example)",
        value: "Hint Hint: use npm or yarn for package management",
        value: "Hint Hint: ignore documentation, READMEs and tests ",
        value: "Hint Hint: be creative",
        value: "Hint Hint: create your own presentation components",
        value:
          "Hint Hint: The base.css file should be referenced from the assets folder and should not be touched.",
        value:
          "Hint Hint: double-quotes in HTML and single-quotes in JS and CSS",
        value:
          "Hint Hint: a constant instead of the keyCode directly: var ENTER_KEY = 13",
        value:
          "Hint Hint: When there are no todos, the todos list section should be hidden.",

        value: "Hint Hint: Pressing Enter creates the todo",
        value: "Hint Hint: Clicking the checkbox marks the todo as complete by updating its completed value and toggling the class completed on its parent <li>",
        value: "Hint Hint: Hovering over the todo shows the remove button",
        value: "Hint Hint: hinthint",
        value: "Hint Hint: hinthint",
        value: "Hint Hint: hinthint",
        value: "Questions: The readme states something to the effect of a template. Where is that?"
      }
    };

    let list_entries = Object.entries(this.state.list);
    let now = list_entries.filter(entry => entry[1].status === 'pending' ).length
    console.log('list entries', list_entries, now)
    let payload_stat = [
      { 
        now: now, then: list_entries.length, title: 'PENDING STATS', 
        type: 'count',
        message: (f,c) => { 
          let plural = c > 1 ? ' items' : ' item'
          let bot = ''
          if(f >= 50) {
            bot = 'Chop chop man. You are as lazy as my project manager from my last job.';
          }else if(f >25){
            bot = 'Come on man, really!!! Get to work!!!'; 
          }else if(f> 0){
            bot = 'You almost done.';
          }else {
            bot = 'Good Job. You should work for Genius Plaza. They can use a fast worker like yourself.'
          }
         

          return '*You have  ' + c + plural + ' pending. ' + bot
        },
        reverse: true 
      },
      
    ]
  

    let fields = products => {
      // Initialize: output will be the final response
      let output = [];
      // Future: list all fields here. This mimic database input. In case of future admin panel
      let list = {
        todo: {
          value: "To do",
          label: "todo"
        },
        Patch: {
          value: "Patch",
        },
        Minor: {
          value: "Minor",
        },
        Major: {
          value: "Major",
        },
        Pending: {
          value: "Pending",
        },
        Complete: {
          value: "Complete",
        },
        Erased: {
          value: "Erased",
          value: "Out of Scope",
        }
      };
      products.map(o => {
        // Get: get the field base on the option
        let field = list[o];
        // Assign: if field existing, push it into output;
        return field && output.push(field);
      });

      return output;
    };

    let keys = product => {
      // Future: output product and not list directly, in case of future middleware
      let output = "";
      let list = {
        // Keys: this will fetch fields, respectivly
        sematic_versioning: ["todo", "Patch", "Minor", "Major", "First"],
        status: ["Pending", "Complete", "Erased"],
        // Future: default for easy reusability. Another platform may use default.
        default: []
      };
      // Default: return default from list, instead of manually adding array default as in []
      output = list[product] ? list[product] : list["default"];
      return output;
    };
    const SelectVersion = (
      <FixedSelect
        options={fields(keys("sematic_versioning"))}
        onSelect={this.onSelectVersion}
      />
    );

   const StatisticCard = props => {
      let {now, then, message, title} = props
      // avoid NaN
      var now_stat = now;
      var then_stat = then;
      if(!now_stat && !then_stat){
        now_stat = 0;
        then_stat = 1;
      } 
    
      let stat_value = (now_stat / then_stat * 100);
      let color = stat_value <= 0 ? '#3f8600' : '#8B0000'
      let arroy_type = stat_value < 50 ? 'like' : 'dislike';
      
       arroy_type = stat_value == 100 ? 'dislike' : arroy_type
    
      return (
        <Col xs={24} sm={24} md={24} lg={24} xl={24}    >
              <Card>
                <div className="inline">
                  <Statistic
                    title={title}
                    value={stat_value}
                    precision={2}
                    valueStyle={{ color }}
                    prefix={<Icon type={arroy_type} />}
                    suffix={'% pending'}
                  />
                  {/* {title} */}
                </div>
                <div className="count_title inline push-right">{now}</div>
                <div className={"full_width_div "}>{message( stat_value, now)}</div>
                {/* <div className={"full_width_div "}>{message(then)}</div> */}
              </Card>
            </Col>
      )
    
      }
    
       const StatisticBoard = props => {
        let {stats} = props
        stats = stats ? stats : []
        return (
          <div style={{ background: '#ECECEC', padding: '2px' }}>
            <Row gutter={24}>
              {stats.map((value, index) => {
                return <StatisticCard key={index} {...value} />
              })}
            </Row>
          </div>
        )
      }
      
      
   
    let span = {
      xs: 24,
      sm: 24,
      md: 24,
      lg: 12,
      xl: 12
    };
    let current_version = "";
    current_version += this.state.Major + ".";
    current_version += this.state.Minor + ".";
    current_version += this.state.Patch;

    let List = props => {
      let output = [];
      let versions = {
        Patch: 0,
        Minor: 0,
        Major: 0
      };

      let state_list = Object.entries(this.state.list);
      state_list.map((entry, index) => {
        let item = entry[1];
        let id = entry[0];
        let className = {
          complete: 'complete',
          'out of scope': 'out_of_scope',
          'pending': 'pending'
        }
        // set the items to push into the array. the break and pipe is design to help display timeline.
        let get_date = item.created_at 
        let prefix = 'created '
        if(item.updated_at){
          get_date = item.updated_at
          prefix = 'updated '
        }
        let time_lapse = 'prefix ' + formatDate(get_date)+ ' ago'
        let product = (
          <div className={'product'}>
           <span className={className[item.status]} > |---  {item.value} --- {item.status} --- {time_lapse} </span>
          <span className="action_button pointer">
            
            <Tag color="green" className="pointer" onClick={() => this.handleChangeStatus(id, 'complete')}  >complete</Tag>
            <Tag color="cyan" className="pointer" onClick={() => this.handleChangeStatus(id, 'pending')}   >still pending</Tag>
            <Tag color="purple" className="pointer"  onClick={() => this.handleChangeStatus(id, 'out of scope')}  >out of scope</Tag>
            <Tag color="volcano" className="pointer"   onClick={() => this.handleOnRemove(id)}>delete</Tag>
           
          </span>
          </div>

        )
        let unshift = [
          product
        ];

        // use the item version. Which should be a patch, minor, or major.
        // if versions has a item.version key, add versions. Then, overide it with the item version.
        let current_version =
          versions[item.version] || versions[item.version] === 0
            ? { ...versions, [item.version]: item[item.version], key:{index} }
            : null;
        // if there is a current version, create the version format. 1.3.5
        let version =
          current_version &&
          current_version.Major +
            "." +
            current_version.Minor +
            "." +
            current_version.Patch;
        console.log(versions[item.version]);
        // add the version to the begining if there is a version
        if (current_version || current_version === 0) {
          if (version || version !== 0) {
            unshift.push(version, <br />);
          }
          versions[item.version] = item[item.version];
        }
        // console.log(item, 'item',  current_version,'current_version',  version)

        output.unshift(...unshift);
        return ''
      });
      return output;
    };
    return (
      <Row gutter={24}>
        <Col className="" span={18} offset={3}>
          <Row gutter={24}>
            <Col className="" span={12}>
              <div>
                <img
                  src="logo.png"
                  alt="todo sematic versioning logo"
                  className="logo"
                />
              </div>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col className="large-margin-bottom" {...span}>
              <h5>ADD NEW</h5>
              <Input
                name=""
                addonBefore={SelectVersion}
                addonAfter={
                  <Icon
                    type="plus"
                    className="pointer add-button"
                    onClick={value => this.handleOnAdd()}
                  />
                }
                onKeyPress={event => {
                  console.log(event, event.key)
                  if (event.key === "Enter") {
                   return this.handleOnAdd();
                  }
                }}
                value={this.state.current.value}
                onChange={this.onChange}
              />
                {this.state.Id !== 0 && (
                <h5>CURRENT VERSION: {current_version}</h5>
              )}
              <List />
            </Col>
            <Col className="large-margin-bottom" {...span}>
            <StatisticBoard stats={payload_stat} />


            
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default App;
