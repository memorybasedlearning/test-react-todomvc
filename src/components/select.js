import React from 'react';
import {Select} from 'antd'
const Option = Select.Option

const InputSelect = props => {
    // Default: default classname if undefined
    let className = props.className ? props.className : "";
    // Error Handling: make sure options is an array
    let options = Array.isArray(props.options) ? props.options : [];
    // Default: initialized onSelect function and default if none
    let callback = props.onSelect ? props.onSelect : f => f;
    let size = props.size ? props.size : 'small'
    return (
      // Default: defautl to patch. Assuming most of the to do will be a patch
      <Select style={{ minWidth: 100 }} placeholder="Versioning" size={size} className={className} onChange={callback}>
        {options.map((o, i) => {
          // Default: value should be defaulted to index if unassigned. Starting at 1, not 0
          let value = o.value ? o.value : i + 1;
          // Default: if no label, default it to value. Which should at least have a numerical value.
          let label = o.label ? o.label : value;
          return (
            <Option key={i} value={value}>
              {label}
            </Option>
          );
        })}
      </Select>
    );
  };

  export default InputSelect