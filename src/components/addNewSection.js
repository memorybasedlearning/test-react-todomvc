import React from "react";
import { Col, Icon, Input, Row } from "antd";
import InputSelect from "./select";
import { GetFields } from "../utils/admin";

const AddNew = props => {

  // Input should include a prefix of selects. 
  // And a button suffix to add value to list
  return (
    <Row>
      <Col className="large-margin-bottom" >
        <h5>ADD NEW</h5>
        <Input
          addonBefore={
            <InputSelect
              onSelect={props.onSelectVersion}
              options={GetFields("sematic_versioning")}
            />
          }
          addonAfter={
            <Icon
              type="plus"
              className="pointer add-button"
              onClick={() => props.handleOnAdd()}
            />
          }
          onKeyPress={event => {
            if (event.key === "Enter") {
              return props.handleOnAdd();
            }
          }}
          value={props.current_value}
          onChange={props.onChange}
        />
      </Col>
    </Row>
  );
};

export default AddNew;
