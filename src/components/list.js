import React from "react";
import { Tag, Row, Col } from "antd";
import formatDate from "date-fns/distance_in_words_to_now";

const List = props => {
  let output = [];
  let versions = {
    Patch: 0,
    Minor: 0,
    Major: 0
  };

  let state_list = Object.entries(props.list);
  state_list.map((entry, index) => {
    let item = entry[1];
    let id = entry[0];
    let className = {
      complete: "complete",
      "out of scope": "out_of_scope",
      pending: "pending"
    };
    // set the items to push into the array. the break and pipe is design to help display timeline.
    let get_date = item.created_at;
    // if updated exist, prefix updated
    let prefix = "created ";
    if (item.updated_at) {
      get_date = item.updated_at;
      prefix = "updated ";
    }
    // display time 'created lapse as 2 months ago'
    let time_lapse = prefix  + formatDate(get_date) + " ago";
    let product = (
      <div className={"product"} key={index}>
        <span className={className[item.status]}>
          {" "}
          |--- {item.value} --- {item.status} --- {time_lapse}{" "}
        </span>
        <span className="action_button pointer">
          <Tag
            color="green"
            className="pointer"
            onClick={() => props.handleChangeStatus(id, "complete")}
          >
            complete
          </Tag>
          <Tag
            color="cyan"
            className="pointer"
            onClick={() => props.handleChangeStatus(id, "pending")}
          >
            still pending
          </Tag>
          <Tag
            color="purple"
            className="pointer"
            onClick={() => props.handleChangeStatus(id, "out of scope")}
          >
            out of scope
          </Tag>
          <Tag
            color="volcano"
            className="pointer"
            onClick={() => props.handleOnRemove(id)}
          >
            delete
          </Tag>
        </span>
      </div>
    );
    let unshift = [product];

    // use the item version. Which should be a patch, minor, or major.
    // if versions has a item.version key, add versions. Then, overide it with the item version.
    let current_version =
      versions[item.version] || versions[item.version] === 0
        ? { ...versions, [item.version]: item[item.version], key: { index } }
        : null;
    // if there is a current version, create the version format. 1.3.5
    let version =
      current_version &&<span key={index+1000000}>{current_version.Major}.{current_version.Minor}.{current_version.Patch}<br />
        </span>
    // add the version to the begining if there is a version
    if (current_version || current_version === 0) {
      if (version || version !== 0) {
        unshift.push(version);
      }
      versions[item.version] = item[item.version];
    }
    output.unshift(...unshift);
    return "";
  });

  return (
    <Row gutter={24} className="large-margin-bottom" >
      <Col>
        {props.Id !== 0 && <h5>CURRENT VERSION: {props.current_version}</h5>}
        {output}
      </Col>
    </Row>
  );
};

export default List;
