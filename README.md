#todo -- sematic versioning app
> A sematic versioning guide and to do app

## Description

### Run
```
 npm install
 npm start

 ```
### Publish
```
 npm build

 ```
### Nomenclature

- patch: small bug fixes. 0.0.x
- minor: minor releases and feature that may not break existing application. 0.x.0
- major: major or features that may break existing applications. x.0.0
- todo: items that must be completed within the working version. 

### Stats

- stats are based on pending applications.
- thumbs up only if more than 50% or not pending.

### Input

- select the sematic. see nomenclature.
- input full description of to do item on input field.
- click on plus sign (button's prefix) to add products.

### List

- Current working version will always be displayed. 
- To down: most recent items are on top

### Color Scheme

playful color to resember Genius Plaza theme.

- #00A14B - closes to color green
- #FBAF3F - closes to color yellow-orange
- #BE1E2D - closes to color red
- rgb(59, 9, 144) - closes to color purple

### Change Log

CURRENT VERSION: 1.4.3

|--- restart patches respectivly. 1.3.7 should be 2.0.0 on major releases, not 1.3.7 --- pending --- created 5 minutes ago 
1.4.3
|--- quality check work --- complete --- updated 6 minutes ago 
1.3.3
|--- show stats on completed work --- out of scope --- updated 6 minutes ago
|--- Chatbot should provide recomendation --- out of scope --- created 6 minutes ago
|--- include chat bot --- complete --- created 6 minutes ago
|--- pluralize message --- complete --- created 7 minutes ago
|--- replace strong with visual stat board --- out of scope --- created 7 minutes ago
|--- show pending list on stats panel --- complete --- created 7 minutes ago
|--- replace css with scss --- complete --- created 8 minutes ago 
1.3.2
|--- show action tags using css child elements --- complete --- created 8 minutes ago
|--- set status using tags instead of checkbox. --- complete --- created 9 minutes ago
|--- remove trailing spaces with trim() --- complete --- created 9 minutes ago 0.3.2
|--- clear the input after each to do --- complete --- created 9 minutes ago
|--- Enter key must create new todo --- complete --- created 10 minutes ago
|--- focus input --- complete --- created 10 minutes ago
|--- new todos are enter in the input at the top of the app --- complete --- created 10 minutes ago
|--- use double quotes on html --- complete --- created 11 minutes ago
 0.2.2
|--- using antd which will be served as base.css --- pending --- created 11 minutes ago
|--- Create app mockup --- pending --- created 12 minutes ago 
0.2.0
|--- add README anyway due to conflict on understanding of guide. --- complete --- created 13 minutes ago
|--- READMEs are usually included, but guide say to ignore it. --- pending --- created 14 minutes ago
|--- include all dependencies in Package.json --- complete --- created 15 minutes ago
|--- Create README FILE --- complete --- created 16 minutes ago
|--- add dumb component into Component folder --- complete --- created 16 minutes ago
|--- Create container with Index --- complete --- created 16 minutes ago 
0.1.0
|--- research on Genius Plaza --- complete --- created 17 minutes ago
|--- Follow React best practices --- complete --- updated 18 minutes ago
|--- spaces out pipe and dashes --- out of scope --- created 18 minutes ago
|--- follow file structure, accordingly --- complete --- created 19 minutes ago completestill pendingout of scopedelete
|--- create react app --- complete --- created 19 minutes ago


